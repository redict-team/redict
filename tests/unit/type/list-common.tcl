# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

# We need a value to make sure the list has the right encoding when it is inserted.
array set largevalue {}
set largevalue(listpack) "hello"
set largevalue(quicklist) [string repeat "x" 8192]
