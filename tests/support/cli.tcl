# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

proc redictcli_tls_config {testsdir} {
    set tlsdir [file join $testsdir tls]
    set cert [file join $tlsdir client.crt]
    set key [file join $tlsdir client.key]
    set cacert [file join $tlsdir ca.crt]

    if {$::tls} {
        return [list --tls --cert $cert --key $key --cacert $cacert]
    } else {
        return {}
    }
}

# Returns command line for executing redict-cli
proc redictcli {host port {opts {}}} {
    set cmd [list src/redict-cli -h $host -p $port]
    lappend cmd {*}[redictcli_tls_config "tests"]
    lappend cmd {*}$opts
    return $cmd
}

# Returns command line for executing redict-cli with a unix socket address
proc redictcli_unixsocket {unixsocket {opts {}}} {
    return [list src/redict-cli -s $unixsocket {*}$opts]
}

# Run redict-cli with specified args on the server of specified level.
# Returns output broken down into individual lines.
proc redictcli_exec {level args} {
    set cmd [redictcli_unixsocket [srv $level unixsocket] $args]
    set fd [open "|$cmd" "r"]
    set ret [lrange [split [read $fd] "\n"] 0 end-1]
    close $fd

    return $ret
}
