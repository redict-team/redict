# Copyright (C) 2014 Salvatore Sanfilippo antirez@gmail.com
# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

cd tests/sentinel
source ../instances.tcl

set ::instances_count 5 ; # How many instances we use at max.
set ::tlsdir "../../tls"

proc main {} {
    parse_options
    if {$::leaked_fds_file != ""} {
        set ::env(LEAKED_FDS_FILE) $::leaked_fds_file
    }
    spawn_instance sentinel $::sentinel_base_port $::instances_count {
        "sentinel deny-scripts-reconfig no"
        "enable-protected-configs yes"
        "enable-debug-command yes"
    } "../tests/includes/sentinel.conf"

    spawn_instance redict $::redict_base_port $::instances_count {
        "enable-protected-configs yes"
        "enable-debug-command yes"
        "save ''"
    }
    run_tests
    cleanup
    end_tests
}

if {[catch main e]} {
    puts $::errorInfo
    cleanup
    exit 1
}
