// Copyright (c) 2019, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <assert.h>

/* We need to store events to be able to test and see what we got, and we can't
 * store them in the key-space since that would mess up rdb loading (duplicates)
 * and be lost of flushdb. */
RedictModuleDict *event_log = NULL;
/* stores all the keys on which we got 'removed' event */
RedictModuleDict *removed_event_log = NULL;
/* stores all the subevent on which we got 'removed' event */
RedictModuleDict *removed_subevent_type = NULL;
/* stores all the keys on which we got 'removed' event with expiry information */
RedictModuleDict *removed_expiry_log = NULL;

typedef struct EventElement {
    long count;
    RedictModuleString *last_val_string;
    long last_val_int;
} EventElement;

void LogStringEvent(RedictModuleCtx *ctx, const char* keyname, const char* data) {
    EventElement *event = RedictModule_DictGetC(event_log, (void*)keyname, strlen(keyname), NULL);
    if (!event) {
        event = RedictModule_Alloc(sizeof(EventElement));
        memset(event, 0, sizeof(EventElement));
        RedictModule_DictSetC(event_log, (void*)keyname, strlen(keyname), event);
    }
    if (event->last_val_string) RedictModule_FreeString(ctx, event->last_val_string);
    event->last_val_string = RedictModule_CreateString(ctx, data, strlen(data));
    event->count++;
}

void LogNumericEvent(RedictModuleCtx *ctx, const char* keyname, long data) {
    REDICTMODULE_NOT_USED(ctx);
    EventElement *event = RedictModule_DictGetC(event_log, (void*)keyname, strlen(keyname), NULL);
    if (!event) {
        event = RedictModule_Alloc(sizeof(EventElement));
        memset(event, 0, sizeof(EventElement));
        RedictModule_DictSetC(event_log, (void*)keyname, strlen(keyname), event);
    }
    event->last_val_int = data;
    event->count++;
}

void FreeEvent(RedictModuleCtx *ctx, EventElement *event) {
    if (event->last_val_string)
        RedictModule_FreeString(ctx, event->last_val_string);
    RedictModule_Free(event);
}

int cmdEventCount(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc != 2){
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    EventElement *event = RedictModule_DictGet(event_log, argv[1], NULL);
    RedictModule_ReplyWithLongLong(ctx, event? event->count: 0);
    return REDICTMODULE_OK;
}

int cmdEventLast(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    if (argc != 2){
        RedictModule_WrongArity(ctx);
        return REDICTMODULE_OK;
    }

    EventElement *event = RedictModule_DictGet(event_log, argv[1], NULL);
    if (event && event->last_val_string)
        RedictModule_ReplyWithString(ctx, event->last_val_string);
    else if (event)
        RedictModule_ReplyWithLongLong(ctx, event->last_val_int);
    else
        RedictModule_ReplyWithNull(ctx);
    return REDICTMODULE_OK;
}

void clearEvents(RedictModuleCtx *ctx)
{
    RedictModuleString *key;
    EventElement *event;
    RedictModuleDictIter *iter = RedictModule_DictIteratorStart(event_log, "^", NULL);
    while((key = RedictModule_DictNext(ctx, iter, (void**)&event)) != NULL) {
        event->count = 0;
        event->last_val_int = 0;
        if (event->last_val_string) RedictModule_FreeString(ctx, event->last_val_string);
        event->last_val_string = NULL;
        RedictModule_DictDel(event_log, key, NULL);
        RedictModule_Free(event);
    }
    RedictModule_DictIteratorStop(iter);
}

int cmdEventsClear(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argc);
    REDICTMODULE_NOT_USED(argv);
    clearEvents(ctx);
    return REDICTMODULE_OK;
}

/* Client state change callback. */
void clientChangeCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);

    RedictModuleClientInfo *ci = data;
    char *keyname = (sub == REDICTMODULE_SUBEVENT_CLIENT_CHANGE_CONNECTED) ?
        "client-connected" : "client-disconnected";
    LogNumericEvent(ctx, keyname, ci->id);
}

void flushdbCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);

    RedictModuleFlushInfo *fi = data;
    char *keyname = (sub == REDICTMODULE_SUBEVENT_FLUSHDB_START) ?
        "flush-start" : "flush-end";
    LogNumericEvent(ctx, keyname, fi->dbnum);
}

void roleChangeCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(data);

    RedictModuleReplicationInfo *ri = data;
    char *keyname = (sub == REDICTMODULE_EVENT_REPLROLECHANGED_NOW_MASTER) ?
        "role-master" : "role-replica";
    LogStringEvent(ctx, keyname, ri->masterhost);
}

void replicationChangeCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(data);

    char *keyname = (sub == REDICTMODULE_SUBEVENT_REPLICA_CHANGE_ONLINE) ?
        "replica-online" : "replica-offline";
    LogNumericEvent(ctx, keyname, 0);
}

void rasterLinkChangeCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(data);

    char *keyname = (sub == REDICTMODULE_SUBEVENT_MASTER_LINK_UP) ?
        "masterlink-up" : "masterlink-down";
    LogNumericEvent(ctx, keyname, 0);
}

void persistenceCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(data);

    char *keyname = NULL;
    switch (sub) {
        case REDICTMODULE_SUBEVENT_PERSISTENCE_RDB_START: keyname = "persistence-rdb-start"; break;
        case REDICTMODULE_SUBEVENT_PERSISTENCE_AOF_START: keyname = "persistence-aof-start"; break;
        case REDICTMODULE_SUBEVENT_PERSISTENCE_SYNC_AOF_START: keyname = "persistence-syncaof-start"; break;
        case REDICTMODULE_SUBEVENT_PERSISTENCE_SYNC_RDB_START: keyname = "persistence-syncrdb-start"; break;
        case REDICTMODULE_SUBEVENT_PERSISTENCE_ENDED: keyname = "persistence-end"; break;
        case REDICTMODULE_SUBEVENT_PERSISTENCE_FAILED: keyname = "persistence-failed"; break;
    }
    /* modifying the keyspace from the fork child is not an option, using log instead */
    RedictModule_Log(ctx, "warning", "module-event-%s", keyname);
    if (sub == REDICTMODULE_SUBEVENT_PERSISTENCE_SYNC_RDB_START ||
        sub == REDICTMODULE_SUBEVENT_PERSISTENCE_SYNC_AOF_START)
    {
        LogNumericEvent(ctx, keyname, 0);
    }
}

void loadingCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(data);

    char *keyname = NULL;
    switch (sub) {
        case REDICTMODULE_SUBEVENT_LOADING_RDB_START: keyname = "loading-rdb-start"; break;
        case REDICTMODULE_SUBEVENT_LOADING_AOF_START: keyname = "loading-aof-start"; break;
        case REDICTMODULE_SUBEVENT_LOADING_REPL_START: keyname = "loading-repl-start"; break;
        case REDICTMODULE_SUBEVENT_LOADING_ENDED: keyname = "loading-end"; break;
        case REDICTMODULE_SUBEVENT_LOADING_FAILED: keyname = "loading-failed"; break;
    }
    LogNumericEvent(ctx, keyname, 0);
}

void loadingProgressCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);

    RedictModuleLoadingProgress *ei = data;
    char *keyname = (sub == REDICTMODULE_SUBEVENT_LOADING_PROGRESS_RDB) ?
        "loading-progress-rdb" : "loading-progress-aof";
    LogNumericEvent(ctx, keyname, ei->progress);
}

void shutdownCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(data);
    REDICTMODULE_NOT_USED(sub);

    RedictModule_Log(ctx, "warning", "module-event-%s", "shutdown");
}

void cronLoopCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(sub);

    RedictModuleCronLoop *ei = data;
    LogNumericEvent(ctx, "cron-loop", ei->hz);
}

void moduleChangeCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);

    RedictModuleModuleChange *ei = data;
    char *keyname = (sub == REDICTMODULE_SUBEVENT_MODULE_LOADED) ?
        "module-loaded" : "module-unloaded";
    LogStringEvent(ctx, keyname, ei->module_name);
}

void swapDbCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    REDICTMODULE_NOT_USED(sub);

    RedictModuleSwapDbInfo *ei = data;
    LogNumericEvent(ctx, "swapdb-first", ei->dbnum_first);
    LogNumericEvent(ctx, "swapdb-second", ei->dbnum_second);
}

void configChangeCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);
    if (sub != REDICTMODULE_SUBEVENT_CONFIG_CHANGE) {
        return;
    }

    RedictModuleConfigChangeV1 *ei = data;
    LogNumericEvent(ctx, "config-change-count", ei->num_changes);
    LogStringEvent(ctx, "config-change-first", ei->config_names[0]);
}

void keyInfoCallback(RedictModuleCtx *ctx, RedictModuleEvent e, uint64_t sub, void *data)
{
    REDICTMODULE_NOT_USED(e);

    RedictModuleKeyInfoV1 *ei = data;
    RedictModuleKey *kp = ei->key;
    RedictModuleString *key = (RedictModuleString *) RedictModule_GetKeyNameFromModuleKey(kp);
    const char *keyname = RedictModule_StringPtrLen(key, NULL);
    RedictModuleString *event_keyname = RedictModule_CreateStringPrintf(ctx, "key-info-%s", keyname);
    LogStringEvent(ctx, RedictModule_StringPtrLen(event_keyname, NULL), keyname);
    RedictModule_FreeString(ctx, event_keyname);

    /* Despite getting a key object from the callback, we also try to re-open it
     * to make sure the callback is called before it is actually removed from the keyspace. */
    RedictModuleKey *kp_open = RedictModule_OpenKey(ctx, key, REDICTMODULE_READ);
    assert(RedictModule_ValueLength(kp) == RedictModule_ValueLength(kp_open));
    RedictModule_CloseKey(kp_open);

    /* We also try to RM_Call a command that accesses that key, also to make sure it's still in the keyspace. */
    char *size_command = NULL;
    int key_type = RedictModule_KeyType(kp);
    if (key_type == REDICTMODULE_KEYTYPE_STRING) {
        size_command = "STRLEN";
    } else if (key_type == REDICTMODULE_KEYTYPE_LIST) {
        size_command = "LLEN";
    } else if (key_type == REDICTMODULE_KEYTYPE_HASH) {
        size_command = "HLEN";
    } else if (key_type == REDICTMODULE_KEYTYPE_SET) {
        size_command = "SCARD";
    } else if (key_type == REDICTMODULE_KEYTYPE_ZSET) {
        size_command = "ZCARD";
    } else if (key_type == REDICTMODULE_KEYTYPE_STREAM) {
        size_command = "XLEN";
    }
    if (size_command != NULL) {
        RedictModuleCallReply *reply = RedictModule_Call(ctx, size_command, "s", key);
        assert(reply != NULL);
        assert(RedictModule_ValueLength(kp) == (size_t) RedictModule_CallReplyInteger(reply));
        RedictModule_FreeCallReply(reply);
    }

    /* Now use the key object we got from the callback for various validations. */
    RedictModuleString *prev = RedictModule_DictGetC(removed_event_log, (void*)keyname, strlen(keyname), NULL);
    /* We keep object length */
    RedictModuleString *v = RedictModule_CreateStringPrintf(ctx, "%zd", RedictModule_ValueLength(kp));
    /* For string type, we keep value instead of length */
    if (RedictModule_KeyType(kp) == REDICTMODULE_KEYTYPE_STRING) {
        RedictModule_FreeString(ctx, v);
        size_t len;
        /* We need to access the string value with RedictModule_StringDMA.
         * RedictModule_StringDMA may call dbUnshareStringValue to free the origin object,
         * so we also can test it. */
        char *s = RedictModule_StringDMA(kp, &len, REDICTMODULE_READ);
        v = RedictModule_CreateString(ctx, s, len);
    }
    RedictModule_DictReplaceC(removed_event_log, (void*)keyname, strlen(keyname), v);
    if (prev != NULL) {
        RedictModule_FreeString(ctx, prev);
    }

    const char *subevent = "deleted";
    if (sub == REDICTMODULE_SUBEVENT_KEY_EXPIRED) {
        subevent = "expired";
    } else if (sub == REDICTMODULE_SUBEVENT_KEY_EVICTED) {
        subevent = "evicted";
    } else if (sub == REDICTMODULE_SUBEVENT_KEY_OVERWRITTEN) {
        subevent = "overwritten";
    }
    RedictModule_DictReplaceC(removed_subevent_type, (void*)keyname, strlen(keyname), (void *)subevent);

    RedictModuleString *prevexpire = RedictModule_DictGetC(removed_expiry_log, (void*)keyname, strlen(keyname), NULL);
    RedictModuleString *expire = RedictModule_CreateStringPrintf(ctx, "%lld", RedictModule_GetAbsExpire(kp));
    RedictModule_DictReplaceC(removed_expiry_log, (void*)keyname, strlen(keyname), (void *)expire);
    if (prevexpire != NULL) {
        RedictModule_FreeString(ctx, prevexpire);
    }
}

static int cmdIsKeyRemoved(RedictModuleCtx *ctx, RedictModuleString **argv, int argc){
    if(argc != 2){
        return RedictModule_WrongArity(ctx);
    }

    const char *key  = RedictModule_StringPtrLen(argv[1], NULL);

    RedictModuleString *value = RedictModule_DictGetC(removed_event_log, (void*)key, strlen(key), NULL);

    if (value == NULL) {
        return RedictModule_ReplyWithError(ctx, "ERR Key was not removed");
    }

    const char *subevent = RedictModule_DictGetC(removed_subevent_type, (void*)key, strlen(key), NULL);
    RedictModule_ReplyWithArray(ctx, 2);
    RedictModule_ReplyWithString(ctx, value);
    RedictModule_ReplyWithSimpleString(ctx, subevent);

    return REDICTMODULE_OK;
}

static int cmdKeyExpiry(RedictModuleCtx *ctx, RedictModuleString **argv, int argc){
    if(argc != 2){
        return RedictModule_WrongArity(ctx);
    }

    const char* key  = RedictModule_StringPtrLen(argv[1], NULL);
    RedictModuleString *expire = RedictModule_DictGetC(removed_expiry_log, (void*)key, strlen(key), NULL);
    if (expire == NULL) {
        return RedictModule_ReplyWithError(ctx, "ERR Key was not removed");
    }
    RedictModule_ReplyWithString(ctx, expire);
    return REDICTMODULE_OK;
}

/* This function must be present on each Redict module. It is used in order to
 * register the commands into the Redict server. */
int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
#define VerifySubEventSupported(e, s) \
    if (!RedictModule_IsSubEventSupported(e, s)) { \
        return REDICTMODULE_ERR; \
    }

    if (RedictModule_Init(ctx,"testhook",1,REDICTMODULE_APIVER_1)
        == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    /* Example on how to check if a server sub event is supported */
    if (!RedictModule_IsSubEventSupported(RedictModuleEvent_ReplicationRoleChanged, REDICTMODULE_EVENT_REPLROLECHANGED_NOW_MASTER)) {
        return REDICTMODULE_ERR;
    }

    /* replication related hooks */
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_ReplicationRoleChanged, roleChangeCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_ReplicaChange, replicationChangeCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_MasterLinkChange, rasterLinkChangeCallback);

    /* persistence related hooks */
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_Persistence, persistenceCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_Loading, loadingCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_LoadingProgress, loadingProgressCallback);

    /* other hooks */
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_ClientChange, clientChangeCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_FlushDB, flushdbCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_Shutdown, shutdownCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_CronLoop, cronLoopCallback);

    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_ModuleChange, moduleChangeCallback);
    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_SwapDB, swapDbCallback);

    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_Config, configChangeCallback);

    RedictModule_SubscribeToServerEvent(ctx,
        RedictModuleEvent_Key, keyInfoCallback);

    event_log = RedictModule_CreateDict(ctx);
    removed_event_log = RedictModule_CreateDict(ctx);
    removed_subevent_type = RedictModule_CreateDict(ctx);
    removed_expiry_log = RedictModule_CreateDict(ctx);

    if (RedictModule_CreateCommand(ctx,"hooks.event_count", cmdEventCount,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"hooks.event_last", cmdEventLast,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"hooks.clear", cmdEventsClear,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"hooks.is_key_removed", cmdIsKeyRemoved,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;
    if (RedictModule_CreateCommand(ctx,"hooks.pexpireat", cmdKeyExpiry,"",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (argc == 1) {
        const char *ptr = RedictModule_StringPtrLen(argv[0], NULL);
        if (!strcasecmp(ptr, "noload")) {
            /* This is a hint that we return ERR at the last moment of OnLoad. */
            RedictModule_FreeDict(ctx, event_log);
            RedictModule_FreeDict(ctx, removed_event_log);
            RedictModule_FreeDict(ctx, removed_subevent_type);
            RedictModule_FreeDict(ctx, removed_expiry_log);
            return REDICTMODULE_ERR;
        }
    }

    return REDICTMODULE_OK;
}

int RedictModule_OnUnload(RedictModuleCtx *ctx) {
    clearEvents(ctx);
    RedictModule_FreeDict(ctx, event_log);
    event_log = NULL;

    RedictModuleDictIter *iter = RedictModule_DictIteratorStartC(removed_event_log, "^", NULL, 0);
    char* key;
    size_t keyLen;
    RedictModuleString* val;
    while((key = RedictModule_DictNextC(iter, &keyLen, (void**)&val))){
        RedictModule_FreeString(ctx, val);
    }
    RedictModule_FreeDict(ctx, removed_event_log);
    RedictModule_DictIteratorStop(iter);
    removed_event_log = NULL;

    RedictModule_FreeDict(ctx, removed_subevent_type);
    removed_subevent_type = NULL;

    iter = RedictModule_DictIteratorStartC(removed_expiry_log, "^", NULL, 0);
    while((key = RedictModule_DictNextC(iter, &keyLen, (void**)&val))){
        RedictModule_FreeString(ctx, val);
    }
    RedictModule_FreeDict(ctx, removed_expiry_log);
    RedictModule_DictIteratorStop(iter);
    removed_expiry_log = NULL;

    return REDICTMODULE_OK;
}

