#!/bin/sh
#
# SPDX-FileCopyrightText: 2024 Redict Contributors
#
# SPDX-License-Identifier: LGPL-3.0-only
GIT_SHA1=`(git show-ref --head --hash=8 2> /dev/null || echo 00000000) | head -n1`
GIT_DIRTY=`git diff --no-ext-diff -- ../src ../deps 2> /dev/null | wc -l`
BUILD_ID=`uname -n`"-"`date +%s`
if [ -n "$SOURCE_DATE_EPOCH" ]; then
  BUILD_ID=$(date -u -d "@$SOURCE_DATE_EPOCH" +%s 2>/dev/null || date -u -r "$SOURCE_DATE_EPOCH" +%s 2>/dev/null || date -u +%s)
fi
test -f release.h || touch release.h
(cat release.h | grep SHA1 | grep $GIT_SHA1) && \
(cat release.h | grep DIRTY | grep $GIT_DIRTY) && exit 0 # Already up-to-date
echo "#define REDICT_GIT_SHA1 \"$GIT_SHA1\"" > release.h
echo "#define REDICT_GIT_DIRTY \"$GIT_DIRTY\"" >> release.h
echo "#define REDICT_BUILD_ID \"$BUILD_ID\"" >> release.h
echo "#include \"version.h\"" >> release.h
echo "#define REDICT_BUILD_ID_RAW REDICT_VERSION REDICT_BUILD_ID REDICT_GIT_DIRTY REDICT_GIT_SHA1" >> release.h
touch release.c # Force recompile of release.c
