// Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

/* Every time the Redict Git SHA1 or Dirty status changes only this small
 * file is recompiled, as we access this information in all the other
 * files using this functions. */

#include <string.h>
#include <stdio.h>

#include "release.h"
#include "crc64.h"

char *redictGitSHA1(void) {
    return REDICT_GIT_SHA1;
}

char *redictGitDirty(void) {
    return REDICT_GIT_DIRTY;
}

const char *redictBuildIdRaw(void) {
    return REDICT_BUILD_ID_RAW;
}

uint64_t redictBuildId(void) {
    char *buildid = REDICT_BUILD_ID_RAW;

    return crc64(0,(unsigned char*)buildid,strlen(buildid));
}

/* Return a cached value of the build string in order to avoid recomputing
 * and converting it in hex every time: this string is shown in the INFO
 * output that should be fast. */
char *redictBuildIdString(void) {
    static char buf[32];
    static int cached = 0;
    if (!cached) {
        snprintf(buf,sizeof(buf),"%llx",(unsigned long long) redictBuildId());
        cached = 1;
    }
    return buf;
}
