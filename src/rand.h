// Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#ifndef REDICT_RANDOM_H
#define REDICT_RANDOM_H

int32_t redictLrand48(void);
void redictSrand48(int32_t seedval);

#define REDICT_LRAND48_MAX INT32_MAX

#endif
